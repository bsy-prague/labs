#include <iostream>
#include <vector>
using namespace std;

int sum(const vector<int> &v) {
  int result = 0;
  for (size_t i = 0; i < v.size() ; i++) {
    result += v[i];
  }
  return result;
}
