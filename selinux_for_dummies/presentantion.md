---
author: Josef Liška
title: SELinux for dummies
---

# SELinux for dummies
## Josef Liška
<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAumUlEQVR4XuzLQQoBUQAA0F8sHGGKPw3ZOQEWpuknF5itc0gpRSbDCdzBGZSNa7mDHd7bv/BHAAAAAAAAAAAAAAAAqOvQ2c6y67EcvE5VvLfLYnNZjaf7suiFH9akfNSm4fqc8ltTxedh0X/s5tkkfLHuJ+nd3peAyVVUbb9Vdbfunu6efTKTmez7BmQBwi5g2I3sCCIo/vApgiAqbii/+vstiiICoiwCfiqCbC7hkxgIW9iyQEhCNsg+k8w+vd+lqv5O9b3PTT89IQJ+GMycPOep2933qdTc856lTp2q27a5tsU08TmNyKjrejAgzmIakbVR7c2fnjrhj54UD173xMYV+BegH84b20gZToYgn5CCHyE8NyEEh6EzRBjQ48ozAazGh5Qo3gNFNCIkdxyzvgktHz0LGDEZGUmJnc9PMcC/FmH0pVvPmLjo1jMmXPyT40ZVfygFf+r4w24+dcJtusaWa1Le5zqFk3NgCTZqMppOPBO0dhgc24GQngcABxQA9FRPlxCyXeQzGDH9EBz+ySsw+7JrUXvkKRgwk0jlCwbl7vEmI/dbCevVn5028Yab5o1tw35ONx53nHbTaeM+dvNp4//HBHmOCu/zuUJheC5SjYYjT8Khxb/x8IsuR9u0Q8DzGTgeh+B8LT7ERPAe6Ztzqu+J6eTTkz7xOYycdRR0XYeUEun+HuxYsxLty1+E3bEFMSYRjVhwuOyUwH2ey39x9RMb33r3pnhGTLfsVk2S8SCkDUCdBAwG2c8hOwkRG/JUbPrKo291vnvBj7KSUf1MRuhVlMi5nusi6wFWy0i0zpyL4VNmIF5TB0IIXNfF5mUvYM1vf44Cl6l+V864bUX/lgMOAF85pOrsCBV/aJlzDOZ86irEYlVgTIMUAh73kE2n0LFhDba++jwym9YiCo6qqAlXkD5PyvuzLv/ZV/cBhB+eMXG0Dno8hZjHCJ1JpGzVGLUYJWrgpDR6BTxXSCEhuyXIm0SSZzjEX7sGCstvXLy5gL3QVaeMM8dSdi4j5FoGMdNxHOSgoWr0JIw89Ei0jJ+MWDwJjTEQysC5h2w2g5fvvxVdS59DRtBFP1ye+igAecAB4MpJ8bq45b2RqKlpnnvN99A8ahxM0wKlFEIIeJ5bZA/5XBY7316PLa88i9SG1YgIB1VRC55EryflnXaB/vTav77ZAZ9uP216DafuKRK4kBFypMFINQEgZEnQEiERUo5kSgBGCGiRbS6kBFYJgcccjge/+Jc3VyEk8tPTJ3+cQnxNIzi0YNvIUwPJ8VMx+rBjMGzMBESiMWiaVmQ9+Jtg2wV0bN6I537ybXjpfmRc+fkfr8z9/IB0AcoKTDfviDJxxcRzLsP0U89DNBKBrukAIUpYgnMFBHc3EPI5dG7eWLQIL2Bg/UpEvAJiEROOkNsciX93pHzakOQiSslFOiWjKSHgQgbKBekPVsq9DJyETWAdGCUKEAUuc1yKJ21P3kYoBhjI/6XAKZ7jIs9MVE+cXtT4o9BYBHEkEoXuC54xVupISriei3w+j9cWPIQ3H7obDmhfIVuY8bON2H7AAuDqSTgiqpFn68ZOZkdd813U1DUqK8AYDUyz0hzOOVzHgW3byOWy6N6xGduXLoG7+U1EpAchCTwpcyajUSElhESFVZXSBwDkICgmFWAIXQRRrQKCp6yCY1CYOWjQR01C6+wj0dA2CtFIDKZpQjcMJXhKWWBhlPYXCgX09XThmZu/jWxx3GmP3H/zan4JPuSk4X1Qx1q8PHKqXJLeuuHonWtXIjbnGGUuAR0BCSF85uBF9lwHhHtI1tYg3ZOE19cJQig0QqIu5xicJCRIKHxJKn4FkSEYpG8xQiCBA/BDB9PlAmZNHRJ1tWDCg+c44KYFLgRYkQkheyJPuTLHsdGxdiVSm9fDE5JzV9yFfwEieJ901Xh8oorht8PmHIHZl30Ziao4DMMEoSU3wD0O27FRKOTR9dZadL76LHLb3gJcW2kZQk0blOTg1qCCwj5CrX8nkkIoF0V0A5G2sWgqgrdu7CRErAhMwwDTNAUEKaQSfjqTxkt334TupS8gJ/HMjrU44SGAH/AAuLgJsfokXk3EzckHX3kjmsdPgWVaSos49+A4DlJ9vdjxwkL0vvYiiOeC6rrSehAlKr8NhxMYelIBgECzyxEgSbkTCDv0m8q+VccyAILnApqBmoMOR+uRJ6opn2GYyhVIKVXw175hDV699f/CyxWQsXHu7ZvwhyEL4NMXxuHzcYLbmo87EZPPvBQRy1IdO56Hvh1bsPl//oDCjk3Kv1LKikx8Ld3N+xqGhJRln/aC5HJRv5MJCABQaqRizkvuyRo+CiNOOge1w0fC0DR1b94uYPUj92Hn4oXISaxIeTji3s0oAMABHAOE5Aj8pkBwXc/rL43pnHk0kk0tSgA9mzdg64IHIVK90E1T+VgJqKCPEqoAEFiBwSj0+eXXgNzrFIBUWAAyWL9Bn36gursVkJQiXRzzm7/7BUacci5qR41X1qZ/Vzu6VrwEQQDPw83lwh8CAH75NgauHoubnb7MLe2vLIY45hTkOrZh+xO/h8imoRsWPM7BpJJkKT4gUgmHkHfQVikrNXawQKDcBShglSEipHJASR8AwWxFlEBg9+zChkfuRevJ5yHaMgLtLy8GT6VR4Fjp2RWmfwgAfmB+ny1xdXrVy+NQOwwDrz4F3t8DZlgQgpfJgkoCoixApfAAVAhahijYhwEoiyfeSf5lFkAGMxXlCgRACbyBXmx78g9IzD4WA2+8AkkAV+CHv2xHbggAg1AxIZK6ejx+IHK5u/qfepgK1wFhRsm0SgIhCEggUEpBiAw0FXuBQAUIKq6BCr8/6DXI4H2HFkBF+0KNlStAEErBe3ehb9EjYI6DnMTzGsNDCGgIAJVEJV4XQJq6ThKE+AFciSEEhLqJgvhz7X0EaxWCr1D+fYNg330rAISWIAAFIFVfxHNAGUA4lhVBbg8BYC907lQYxMOtjCDJfRHIQBDqytc2X/iKlY3ei0jlXqN+X0CVPj8QaGhVJFSDvcYBQX8hQ1agSgLQCb5w5Rj86ba3sWgIAINQcwETdANzggcGorhC64J1gjAa9wUo/UhccATIUbkCyrAvklK+4wwCQkDKoF+izDsIDe8Ntb5c8mpcJZBGNLACx3xgCACDEtMwxWDQJAlStQFXCouEIFB3cM+FJAxasgGRumZoZhTctZHvbofXvxNEChD27ocquQdJGfSaZlh1LWCaDreQhd3TAS/VBSI5CNNLYynyoERKMxZQgFFMC/JT+Bchin8UEbQxVv7g/H9711gp1PQwOmYmplz8TXzkaz/HvOt/hnlf+THmffWnxc+3Y+InrofRMgHCc//+5y6lut8aMRVTPvlNHL+736/evLtf1f+x19+OSRd+DdbIGeDcqxS+wi5RLBFYAYAwNJ8yDsaQBRhcUZJBYEfgPzgCYG+ZPikgqI4R8y7GzPmfRH1jE6iyDCLwAKitq0XbuIkYPesovPK7W9Gz9AkwxvaZOeRCounIszDn/M+hqbUNTHmYML1c39CAkROnYPzcE7D0kV+h/ekHQCUPu1UX0v871JXvEWSsmSkA2KigIQvAg4AMIRD2FokrIbXN+xSOufhKNDU3K8gIwUEphaGXFmMMw4BV5JHjxuPY//MN1M08SbmLdyLueWg8Yj6OveyraBs9Gqaug7FSX6Zpqv7V/wOJ5tZWHHvpNWg+7nwIIfwRB8Im8C8UB+uMljaoGRoCgAS2CpAgwgcCRvnDDHxz1fg5OPTMS5FIxAEplWAsy8KObVvw+B8ewO/vvxv3/vJ2vPri8xCco23kSMy9+FpYrVMg+eAgkJ6L2NiZOOKiq9E8vAXpgQEse3kJ/vjwA/jJf3yv2Oc96NzZgUgkooAAADU11Tjs3M/CGjk96BcE4Xj3zFZKkN41qwdPAw8BgGCFK4lHqCrJesd5vqAaxh77cQxraQYBVOmVaVl46skn8HSRD5k5C4cfeTSe+NOjuO4Ll+P6L34OG9auxoRp0zF1/mchmFXpt6WENGOYceblGDNhAl5fvhQvPvc0WoYPx9Rp0/HCs0/h5h/9AF+84lIsKv4fViQCRqkaY8uIkRh9zMcgwCoXFAOLVgLAm4sBbwgAgxC38SYXZA0BBUFoBfbUpsD368kmjJw+S1USa5qmNPLF559FJjWAz3/xOtTV1+G/vv9NDPR3IlplYtmrL+LLV30Ob7y2HLNOOB3VE+dAlFsBFfTVTTsaBx/70aLgF2Pnju0485zzEYlY+I9iX7lcP+pqE+jr6y72/R28/MJziMZU3Z9yM6NmHAoWr1VAIiCh4H3mEhAgT2JwGgLA7iyZkPhvLgEZCn3Q4M9I1qOmoRGMMQWC/v4+rHnjdcw/+xwIyfHb+39ZFODbqK2JIxEzkUhEi4Lrwve+/XXYro1JJ5wFSfVy7dctTJ13NnZ27MCS55/BSaedpub+v77ndvTs2oq66irEqyzE4xY4d3DXHbchl80oEFJGUds0DHq8VgWh4UTALycDQYFjZ4HxBUMAeAfKe949tie277XSQEX5UPN8wzDAKFGm/43XXoPHXcRiUaQG+vD6ileUiQYBmMZgGBpMk2HDhrX41Z13YOKhx8BqHAnJPR9THmIt4zH2oMNwx60/hWWZiEYt9PZ2Y9XKZYhEI5CASgBRCjAGbNq0EatXvaECQ0YpjN2tEanIYoXhLG65YyU6h1LBPt0I0J1jUW9FtClUioOlJDMoRYsEiQkZZkvoYMu7wgOBBCUUusbQ3tGORx9+CII7aGsbge7ubtgOByHCj+w54okERowejyef+EvRtF+ApimHYkvHBhXhCyEw/KAjsGNnB55/7hnEIgYIFWhqbEJfXz88jwNEqPuS1TUYOaYJq9dswOZNm/CR409QswJIAcn5YOuR8IQEAznrq9PZTCHlBinJcsm1lQlmb75xNZwDDgCXjoLVYWlfN6n8gkVRa1CqxC1Q8pVSynD5loSrgIFLcHNpOIU8KCVFpkpAqVQa991zFzRdg6FKxkhZqvhL11+Lw+ceiZt+dBMWLVqIQ4o+e/NTD6h+JTPQNm0OnvrbQqTTKXgOwz13/kKVd+uGDgKVeVSm/vorr8HM2YfiZ7fcglwuB6asAoGdz4MXMlBEZGm4/kIRIKExOdsAmQ3/b8zCkz1Ce+rfJtNr7njTWXVAAWB3Rcylo7yfmBG2zeXiGkYwlQW1uyXdBoQACFWaKINg0LcMzkA3Bno60TpypMJEIpmAwwU0qsHjsshuRWKnYNuw7SwkoLT82GuuBrWqACcDLVqLSF0jliy5B5wLuB4BpRpcLuHmg75KlT+pdAq57ABs20EimQT8euOBrg64mV6AlpJREOECkVoqhgCk9DedICM4flOg/GfD1+DNA9AFKBD0A/yuz0zE7xMclxoUVzOCsYT5kvYBIQUB8R2B9EvAvEw3dqxbhUkHHQzuUgwfPhweB2zXCR56wMo6jB07Gr///UO4885fFU33WtQ3DkPeE9CsKjipLkQakhjI5rBq1Wqki22+oMrNylgIgcmTJuDRRx7HXXfdi63bO3D+Jz4F7u9g2r52JXi2H4xRX+hCtYDwF6kkXI6MJ+WjLvCjn6/DyqGi0JDwzfnT2qzswEv59m0tngCoTgBClZ8HLc8MCtdBw2Gn47xv34za+jp0dnbhxBNPws6O7YhETEQsE1WxWJEj0BnD4YfNVNq6cuVqFShGIxGcdvJJ6F23HCLbBxavR2LMdPz5iSdgu67agJLN5oucRb5gI1O87usfwLhxY5Tmv/TqSkwYPw5PPbUQtbU16Nq5C7/71hXoX7kYVNeDGgE/LpAQHqBHDaCm+as/eHrLD1FJQwB44uFffMdk8jvtq5aTrS89g4HNG8EdjmDldc+lf8IIzKaROOrK76Jt7BgVD7z00kuA5Kirq0UsGkU0GlERuqYxSAkVK+iGEQxYlZsTrVReLpWgPBiGDkhASAHB/c0oXCj3kc+XQNDXN4Bdnd2IViVwzjnnQDMsrF66FMv/+0ewu7fByecgHFdZDEoZrJp61E+YhtbZR0CvaVyTzaaPO+8z3+gaAkBIeOrxu46MJxMLdU2LOJ6HfDaNniIAdr6xAv3Flrs2mKHDSlQj0TICNaPGI1lsraokNMOArhuIRmNgmjZoChagqgnSyvuiYG2/1ASLQDL4VQV3LneRy+aUqXdsW+UfCtk07FQ/nFxWuQbNiqKqoRGxZK0ao84ourv77j7+Y5/+7BAAfHr88bvjTbHYszW11QdzziGEvylUaR+HUyhAeB6opkHTTVUerjFN+VpKmZrGUVZyFYTSYMMIKCF75mNB3tVwlbArCk+UpZDqAgLKzysWkiuLwXng7wXCGYj09zKU4opCIS+6u7rP++iZlz88BAAAzyy490fDGhuvk3sKSz27sNbOV91S7l09TBYKnRCfaaj9QT++JQhHSvYp+PJGhiAQEoBiX7iK1TfCD/Kk9AXvB4ABWFC+Pxk9vT1bM7m+ufPmX9V+QNcD/M/Dt58YiVjXCCmUoDgv6ZWm6ZSqADAQaIAB6jMpXyH0N34G9wUFGEoA6pKUCWFfJBWHwlMt8b8NBR9WKIXdqmtJSIBZCKE2h0ruuUT38xM11ckRRfdxE4ALP+zVQQzvke77+feHGxp7RNM0I53JrMqk03/KFQq3FD+PsCyrRUqBcFUgqBNQn/dSoh1OHkHKCjJD7Sd7M1yy7FIEwvdb3xKEMYEUQJFVC79eMLwvuMdnwPMcNz2Q+upAKvV8Npez8/m8xTk/Yv4px+588LGFyw5IF3DfLTccEqmKjOWELH1kwfJtDz30EF/42C+m19XVvBwpEiQC7S8z8aH2AyC0/Df/M+ie5p8GAFAWZfBRhwCSoeB9jQ8ZUoQtAiDsIfSK+/1rCOzq2HXb8fOv+AIAPPqrG6s1Mzm5YBdaRzu1j82+4gr3QJ8GKnr6L3c90NTQcH6pumbPZVUEAlVtIHjABwD1W8JCQKigkFQUZ4TgqTQAQorQ5EOoRgb+HaHWS+G3IQDKwQKg/Hugv78/09vdNXv+xdevG1oMGoT+8uBPDjZ0bb7j2oH2hDIiBKEbIOWxAGWla5WTF0WmQPC73BME1I8A/M+yrD630uwHHAhdcAjFJZMffCdlmbAH3TcAECTiVVX5XPYLAK4aAsAgJLj8kqkzq1DIQwqu2PM8xcExMVJKQSmjlmWVlmF1A5RpYD5TqoGUZgghSAIrAaFAAIRg2pvZF1L6uXxeivB5IHyuqoC55yqWwlPHvjm2A8/jglJCNE0jjDEUW8WUhhaLMnbJA/d+7+YLLr3hLXzAJKWkhBABn1YvXThiyqwXthNyo9i2ZEnETfATx0w7+k/rly2cLBlNTTz4hB0fGADu/M9rJvf1dH0kn02v49zbLgTfJqXczjnvIB7fKagckARZyYWj6ZpOgJGUsjmGYc6LVVXNSFTXwozEYOilHIEUDJSxkmsQDKGLkEoYMgBAONcv8/mhiefgvvC568J1bRTyWRSyKfT29GQcz31KeGIRpXQ1obSfUkaopkcJSFzTWT1ltFmjWosE2jTG2na31MOnAdzwQUX/m958ehRh1ic7li37MYAcAGzb8OJFnueJovB/t3n9i5OJpt2iEf3Wt1Y/cxF3PD3tJX/zgVoAV9g789nM3N6lG7tvvHdxAfumlwE8eOmlo244dPIp88jGjXePnjS5PlkEgmVFwaQOKoWyDoRKBQLqxwRByZYklVvIgkRPkNxRms89uK4DO59DOt2HnTu22unUwN0DvanbvnHTQ2v+/gOyz2UnzZ2ajCS0uL+y/b9GGzYsMKljHRWJ18Qt07w1n8//tGX2rFzHphdHGZHE1xzbnUc8e077pmVXRKLRH7iu+0bBzp9MCO2ecMgJNyjgPP20Neq4Z5zdFmIfQeA/l46OY75RFfndtKPmRI49fR6amofDiqi0sJ8w0nzh7xkghpnBMLET+nwRmHzPhee6yOUzqqTsjSUvYfmiJemebZ2nLk7jeeyH9OCD57LDDr7u9kgsdjbTtGpCaFpK8VlIzCMEZ8UT8fp0KvOYKJJpmWfx0tb2nXYme1/z6Jlfa29fPcLODJzpOoXtr63pfuy8887j+y0Azh6TGJ+z+XPZgtuUt11MOXQGTrvoLLS0jihZAo0pwTM/NiDUjwloOPQ9c/7Cj/CF8CC4p4Sfz2fQ29OJVxY+hWV/fR4RnSJq6bsYxem/XjewFPsRLV36p2hzzbBbqpLJyyRIEN+4BIRZEYs6tgMuOAih3DB05jiuilGEx3s49x4QnE8jFLOy2cwDWSdx5dSpUx0A2C8BcNU4mN2IL+BCHp+1BQoOh82Bg46bixPPnIeGxhYYpqUCwgAANFgrCDOJYQQPpQkKAJy76oBKu5BHf183Vr74CpYtWAxD5JGMmrAMCi6x2ZHkpDtf716P/YA2vLFobCxec0dVPH6iy0XZBptwL2WQIkf5tnkJGEYpYO3ctWuBs77nrPGnnmoDwH4LgMunVd9oMPIdx5NwXA/SiiM6YgJsR2LkxBbMOnI2qmvrS0e1+gc3DuYGQvMfCJ+rknHbsZFJ9WPj6lXY8NomVYXsbFkF2dtRqgQmQNaVy4nUT7ppWXs3/kn09NNPa+NGRi/RdfO7kWisxXE5KA1c3d4EFGA/qHQSYJQin8+9zpE7vq3tiN53EQR+8HTl9OpjTINdr1GCCOPQG+rROPdEMCuOXCqLgd5+bN6wCeOmGIhVxcFEAABaShZREmqClAFDCO6bfgf5bAY7d2zD9g3taBs3AY2tw8Cmj8fOZxao84uIxpCkZOaA495x3HG4YPFiePjgSAV6EVZ7Boh+rRWJHEEIga1MOvW1WwCS7H2qIVE26+FSSirRWciIWQAW7rcA+MzE+rhhip9GNWJBSOjxGEad8nEkho8CuIBju0j1DqC3axd2dbSjubVN5Qx8F+BXGNGyXcdBskctQ3MXdqGAvv4ebF2/CcmaYWgZ2YpEbRKaocOaNx+b//wA7N5uSMoQ03H2jFTdlxaj57/wAdCbLz9SF6tpPpdq+uWGYR7CNE2dpRwkxsLs49+zC92vVxRcZc8LhfyMfN7dBAD7LQASUfGVKo0c7C8FoPXoE9EwbhJ0ZeY1SC4QjUaVeLvbuxCJRVFTUwempoR7povLASCECKZ8yGbTaN+yBV6BoW1SK6rrahCtigEE0Ia1YPhxpyoQcNeFRgBLIzf820HVC+94vX/F/56p/5U1omXcZ3RmXheJRsZIEHicqzGQ4DidIPu4T+Gr38qOtbFt28vnsp+dOuvEjfstAK6YWjVFY/iiGj53UTflYDTNmK0OZNT9rCAhAGMMXpOHXCaDXe0dMAxDneINQsJ8QLkF8M0/V4c693R1orejD/WNI5Twi8km6Kah7oML1I6ZiMwhc7HrxadAmIYIQ5XNyE03Hod5N/4vuILXXvnj1HhV7c9j0aqjBQhsxwvWQCARZjTDC4l9UFnCC4Ijner/1pSZ8/4MAPstAAyDfdtiSEghYFYl0DL3OBXpa5ruz/v9jJ9loioZR11DPXZs34ye7k7UNzSp4A06AwOFDCNild51HUdN+9KpFHbtaIelJ1E3rAHxZByGZahAUkoK6QOmaeZcDLy1FvnOdmWKLIaP7OpJngkMPIR/IC17/o+HR2PJhyNVsZbdgg/+RgmUFaW8Wwqqlonk6O/r+69ps+b9JwDstwC4YlpytkFwpvJx3ENDUfMTTS3QKAuEHwBAtUUzier6WqQHUujZ2QUrGgUDw9qV67B90w7kMll1X7ImidETR2HUxJHqTN/Ozl3wckDL8Bal/UUXonL68P0rYwxS0xGNJ9E4+0hsXvAQCCQMCjAqr7t8Fh775TK4+AfQkoUPTo9ErUcsK9JsFxw1XmWtJAKf/17XBdSYBffy2Uz239e8decPAGC/BoBBxdUmo4YUHGaiGg3TZqmNIFql8JVWE9NQVqB+WCOyb6fw9rqNWPzYc9iycQdQvhAISoE5x83E3HlzkOoeQENdK+p3a391AqZlgqpaf19rKFUgYJyidtxk7CqCMNuxTVkBk8o5TqHqKCDzNN4n3f/DL8ek9O4xI0XhOw4oZSpgyw2kPd00WfF78l4AEO66Ev1FwD9dKGQjra2XJYGHevdbAFw+NTKCEvmxQPtrxk9GrK6hVBjK9hQ+8dvQCiRrq1GfasKfH34RW4vCt0xSPgPw6/2WP7cCifooxo6chMaWZmU9olVRtdUs0BoBAgoKyUrFqFYsjrophyCzYwtACUwKmoG4AMD7BsDyvy367sgp02fbjqsAl+rpdlY+s9jlXNhHn31uteDiXco/kLvKeUjBueHZzgmCi7uPOPLk3v3aAhBBTjcpksoEawZqJ06HpqqBw2rbgCkNK4N0Ayp6T9QkwW0OjaH0O8pJUgLhCjgpgWEtrahtrEM8UQXdf29B+fmEAPWtAOUeqsdOwo4lcbjZNAACBnn8vCbEntyFLN4jXTy55kpw7xrNNOVAT7e37qUlzhuLFrJUb592wXe+q1HGKBf83bkAKcO/A5J4rm1lctmLDzvq438BgP0aAIzyUykAySWshmGIDxsOprScBcKuBIGSlAbLMlUg19DciLVvrAFTv5U9F8W6QdHaNgK1DXXK9BsRSwk6SKGG/SNwNWCUIVpdh1jLCPS8+ToIY9CoHDU8rk/ELnf5e4p1Dh52KqT4YbyuLv/WimVizaK/6f3t26Ou55FDzvh4tnnM2Cj33HcTA4QnmvpTPu65yAz0X3f4R879LQDs1wC4cARqCHAIiIT0PMRbR8GMVoHSsCx8UFaaTlTyJhavwsGHz8IbS5chm0qruGHPAmLH8TBywhiMnzoJiZoErEhE3RNOESsBQEpxh+o/MXIcutesAJEEOpEaIWIGgHcNgM/ObJ5lUHofBYvkO3YUlj+03qICzNA1WHUN9kHHf1SXQhAuZXhW8j6pfD+D5B4G+vtvOPKE828GgP0eAKaB0QyiCbwkhPjwEWAsKAPbUygoFxJ8S6BrKpKfNGMKTrvgLPzt8b+gv6snSJyAagyjJ43FSWd9DK2j2hCLx6GbwZYxlVELHrZi/zq0AoQg3tyqqpOE54BJuZsnvGvhz2gabRHygKmReiIJiJO3TMYgKFDIO+KwU071EnV1MbuQVyG8bli0SPsM+AIOzj5MDaS/c8y8C74PAB8KAFCJERokgxDqIUfrGkCAwPejRCRkRSQUmH96R01dLY6ZdzzaxozCujdWo6ezS7mQ5tbhmDhtMlrahqMqsdv0mwgerBAlYYdpVeKzLDuv2ErWQovEYPfngzva8C7o0+OGNZiMPGBpdBxRLg8gfq7CdR00TppUGDfnMHP7hnV2365dmQlz5iSYJqmUfB/RvkSw58K18zKXyXz92JMvVvP9Dw0ACNBIUZr76pEIjHgyPEYOCDW+0gqE17qmrIZhGkhWJzG5KHDHcZQFME0DRvC6N13zq4YAoczsYNqPEocggxaJQo8nkO/dFZxbXIO/ky46rDZhOeS3ls4OVYBX/YbJGhqJueOPPMZb+qc/ov3tje6Jl36mStdNnXuun9kkg/r7INPHGEE2nenN5XJXffSMTw/q8/f3INCCFEoo2m7fbFj+w0eFBSinPQBCfXOtMyXsaDwGhOvjZYAKTvwmqi3LGJRbmxAYYJoOPVYFpZBMuRbj75rezmqJUlveHzHoiYAELQ/s1ApdVUNjauWCP0azqQw56aovWtWNw0zbLoC7ntRNgzCmDS58KZR76u9LvVbIZC87+ewrVEzyYQNAoIzqgvpr+3vd56cEUokHAoLw/MHy/YPhhg5ACnUVypuUczkYys8AZIapcEpKXYq/73h8/suIrs2XEmDBGQjBKSkSoISS9LYttTAj3rGfvsyLJJL6ptUrCzvWrnPGzJxFh40cVcU5B1D+ChsFHsfGQDb7666Ovi9deMWXuwHgQwkAIVCQwldYWqr/f78UWg9SVsP/Xkn1pQ6bCjCFwr6EX6M13WFp7CIJCUZocMRs6L9LMADTDbd17tx0+/p1kVcefdjp2b6dTjvho7R22LCYx73QYoRaj1w225fJZr7+sXOv/AV8+vACgCMlBEAlABEKbFAKonZZtg/UN+kEMnjGMvjZN5dlm0IQkizn8h/DjoL3AkihGgiO3ncSfpI1/tLS6CXBNnaKwKsgHCNk8CXduuSFRCGT1iVhYuYZH8tPPuroSC6TEWYkypjGAncGCon+VPrFTCp15TmXXLcCHwDRDwAA3S4HhIS/K0cO9maoQYUTmsZyExmwCK6l38OeVTLl/VSiQpbfLzyuAMB3M8cuDEKnjIMZZ413WRq7xH94vksqe7lVmQdiQmjMc/RodXV+4ryTBqiukyUPP5xO9fRkAalWMKXgyGfTXm9X54/fat8xr0z4H3YL4HHs4hy2ZDC540JwPshmTOUzfd7zO6X5IQgq3h5KAoiEwKgAS9lvIfu9BYtEnuOUQMoBLvE2yglntLRE603vzgijFxIE0b4y85Xb2ZW1CkMONZFjOn/r+edjjhDi8PPON4suIOJ5roodUv3ptflc/kvnXvLlJ+DTvwwACgw7YwJ9oBjm5nLwHDsM2iosQCDUMuEFt4R3BLcFYvTvCxllbTl0Ag6BwT0XdiqtLIAtIV2ONWXZzOnJGlO490V0dgYJNL+sYDNIOAHSHyCR4Q2UAE5/X5VZX2/POn2+qG9ti3DPhes60s7n7+rv7frmZ67+9y5U0offBTy6A32cY7OUgJNOo5BOQfgJjnJBV2px+F1Q+6Za9VmEnwPXErIIryv6Kbc0ahxOPo98by/UGD10pXNYH/r8mhGmMB6PaPQMIMwflK1FlL/itnz8KI1PSybTiRGjc+0b1jtuIU+y6YFNqYG+88765Jcuf3/C3//zAML18LrQcLjI2sh2daKubRRkmdAqzTUEIKmECiDpIFkygpAqBCwqwDAo+y+LzPX3odA/oATmcax4IoMupfmT6meZwK9NRicDBJRAsQJC2fuP/O+IhAzcgQjdjCcBN52ODLz2mnbI/PkFx3Hu7O8f+M4V1/6gAz79KwMAnsCzjocrmAT6N72N4QfNgmAiWKKtAIAQEpTCP6qN+os5CLJ6gZ8tz5kDexW6qASB6pv7AOjfugV2xlF+2/XwJwC4YGLtOSYlPzcorQ/MOC3bjRQGf0AQAwxuCQDAcbnWfND0hckRbf95/qXXL4JPBwQA8gLPmS4GYhTJ3nXrUMhmYDAGJrRgydZn+MIXEIKGICAEkhIQGSaKwor58MGXgcjX8L2CwK8idhwbnWvWQHhAXqI7x/HkhRPrvmdQ8nWdEUaU1pedURT4+bBuP4xQgybABBwuslzIJwVjt08/47Kng716BxQAHu/FtgtrsFgamJ/a0YHeLZsRnTodGueQlEIqKxCYe9WWgwAAKRXShcFWRf48FH6l9ovgOmiV9nucI9XZie51G8AlYBNtS3xY8naLkRMpKc/sAXueRRAMQKBs1u8L3RVK798UQj7icvLbX63epYLKe847D/sbMXxANElHjhB8grgSIAKN06eDUVaqCiIUAKk4DFI1ZSTLzKzP5cJH4AJQLngRmn4huJp/29zFxsXPoGP5atiSgCTjLRFLG6OSOyQ80SScNQRcPouBL3RPyE0ul38oXn9jZzf71u827vzra12ZrgPghJB9UyaNv7IEluk6Zu1csRLdR72N5gkTwTymBM4Iys0+JRCislAEkEE76No5gDLtD1uh2uC0EtdzMdDZic3PLIHLAR41EInogJD+ieGAQHCEXPkraSkp/eYJ2IBcJyQWEUEW2K77ym829qZQSUMAeAKw57v4fxrwiOE42LDgr0i2tYEGh0SBgLIQBEHgF3IgeKKusRcXAFS4gLD1N4+4jgPbc7H+yacw0N4HV6cwEpEgmCy5ICV0ggABQkpwyDyALULgVUrIYgn+co70bnhony+NGAKAosfzePws4GFNw9k7V6zB2888j0nzTgBxHBBDDackfCXkwViGrqJM+wdzD+FZQUJpf4kd14HDXWx7bSU2LloCDwRadRSazhQAvFIXBQBpQOwghLzNJVlNiXidC7LadOKb79+8uTB0SNR7I5HJ41piYlaMYdTaR59ArKkRbQdN93cOAQwMFP4BUaHm+7xnnUDZek7l28BDDs2+68IucueWrVh238PIpV14EXZfJMJW2a5IEyJ6ANLNQTsBp6trY6av8jVxPRg6Lv590kkajo4R/DECVEcakph9xafQMmUSDPWGTxPMPw8gXP/HYGcEViaHUG4BpK/93Df7jueia/sOPH/bfejauB02xc1/FbgWBzAx/BPoLYGtrQJLKcEJPGMndq1aC6uxFrHGekCIMnjKQef7AVcGf0IgDPZESesdx1Y+v2PDRjx366+xc1N73gFufFLiWzjAieCfSEdo+GaC4/sxCRgRHRNOPwGTTjoWVckENMrUplGNsaCEOwwCgcpUcLhjNtB6eJ6r5vr5fA7rn3sFSx9cgL6BDFyTdMdi9HNP9vJHAfAhAHzAdG5LZITD+Q968uKcTEGYVa5AUgI6gIbxozD59I9g+IzJiFTFwEhpA0fwfgHibxEHKa85C84ICDSfC6HOCGpf9xZeX/AM3l6+GgUCIMqQiDBUWxQRRhaBka/f+3bu1SEAfDBAk5eOrbrAIPgvIdHWb3P05zlStoB0BKKugCkBixA0ThqDUXMPQcu0CYg31KliUEZZ+EbPUrfhrhkof698faa3Xwl+3YsrsHnlemRdFyzCEC1y0mJI7Ba+RmFQAlfKrMPF997amL3JD/goADEEgH9c/wwAHQdYH5mYvMFi5EsaI5RLwPUEcq5Axi6BIFtsebFlLocmAB0obQsb06a4unUYksMaEElUqUpeEEB4HgqZHAZ2daN3xy7s2rQN7W9tRW93n5KmEWWIRTTELYa4ydQxcaZGwGj5YlLOlQ9v63KueaI3H1QDeQDkEADef5BpHmKhYc6o6h8nTO2ssKK3ZLq5AFwukHcFcjZH2uYKCLYrIDwBFFl60taAlA5EzaqobkQtyjSNAqCcc+HkbS+bztoFKfs5kNRNmohENFSZRbYYYgZFRGcwlOAp6CB5BAKJrC2Wru8uXP63rtw6AA4AbwgA74/0yVHUHz2y5vbamPHxMIlTKQAuJVxPouBy5BxesgoFzxvIuTuzBb5WcDlAAAuACUD3TTXzzbUtgbwEcpSRaCKiTWpIGG31VYZZZTKYGoWmKpL3hviw0mgg7732ekfqky/0OhsAuO/TCgxZgHlNkdkTGquWJKMGrdwFE9b6CSHheAIZ2xX9OS/dmba7elJul+3yvF+HoclwvEQCci8I5hLwTI0Y9QmjrjFhNVbH9GTUYJqhUVBCFJe/jCq0SI7Dsaknd8kftqZ+A0AMAeD9kd4CJI8fW/P9urj52ajBWPDIPSGU6Xe4yNuO6EsXvM6+rNPRl3H7Mnm3wEsC1kiJCQAqESix4uAA+XCl1m/9nzgHPALIKoOZ1XEjWRczhsUtbZhlsBpdo1VGYBmgSFmf/pzz5Ktbsp97o1DYAcAZAsD7I9YCmByoOrQlfkQ8os/UiKxxQTzb8fqzebenL+elMlm3kAeEBJgGaLtZAoyWhE59jaeB8Png2SwpfAAQQOxm4bce4Mkiu0WOACRmMbM2ZsbjFq21LK1GI8SSQCZT4KtWbk89mwZ6u4DCUAzwj+mfjgL0zWHJL/NZbyiyAWh6SehMB6jwNV34gteV8EMGgKBF5bulZMBuCAZJS0CQHsBdQGiAlwLcLsDz/TwPo34Q/5oPzQL+sUAg4XXI4wCy0f++FSDCv6/ebzvKhY46DE495WBAs992+y0FpAbIzVAkB+fwt6E8wH4whvc5TrnP3w8gGqIhGqIhGqIhGqIhGqIhGqIhGqIhGqIh+v97DSwRDzd0oAAAAABJRU5ErkJggg==" alt="foxy"/>
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/phokz.js"></script>

---

# Let's assume ...
## Most of you allready know:

* What is selinux?
* Why do we want it?

---

# Let's check ...
## Looking for volunteers

to answer theese questions.

--

* What is selinux?
* Why do we want it?

---

# Were the answers correct?

---

# What is selinux?

* brought by NSA :-)
* provides access controll beyond traditional unix user/group/other permissions (often called DAC)
  http://en.wikipedia.org/wiki/Discretionary_access_control
* MAC (Mandatory access control), in fact this buzzword simply means default=DENY

next buzzwords:

* TE (Type enforcement)
* RBAC (Role based access control)
* MLS (Multilevel security)

---

# Why do we want it?

* it brings a way to "contain" a process, or a user, so that it can access only necessary set of resources
* it can help in protecting from impact of bugs in applications.

---

# SElinux -
## how to learn (minimum subset)
## of its features?

### Option A) Spend a week in books
### Option B) Ask (good) questions and find answers


---

# My questions

So I ended up with a bunch of questions that needed an answer, lets see, how many of theese questions
can you guess.

---

<table id="questions">
<tr id="q1"><td>1</td><td><a class="b" data-val="How to tell if selinux is running?">click</a></td></tr>
<tr id="q2"><td>2</td><td><a class="b" data-val="What are main objects of SELinux?">click</a></td></tr>
<tr id="q3"><td>3</td><td><a class="b" data-val="How is selinux implemented?">click</a></td></tr>
<tr id="q4"><td>4</td><td><a class="b" data-val="Where are selinux (meta)data stored?">click</a></td></tr>
<tr id="q5"><td>5</td><td><a class="b" data-val="How to test what would be blocked by selinux?">click</a></td></tr>
<tr id="q6"><td>6</td><td><a class="b" data-val="What default policies are available from vendor?">click</a></td></tr>
<tr id="q7"><td>7</td><td><a class="b" data-val="Is is possible to fine tune available policies?">click</a></td></tr>
<tr id="q8"><td>8</td><td><a class="b" data-val="How to switch context of a file?">click</a></td></tr>
<tr id="q9"><td>9</td><td><a class="b" data-val="How to create own policy?">click</a></td></tr>
<tr id="q10"><td>10</td><td><a class="b" data-val="How to list available contexts?">click</a></td></tr>
<tr id="q11"><td>11</td><td><a class="b" data-val="How to turn denials in rules - use audit2allow?">click</a></td></tr>
<tr id="q12"><td>12</td><td><a class="b" data-val="How to remove previously loaded policy?">click</a></td></tr>
</table>

---

# How to tell if selinux is running?

really a simple one

    getenforce


---

## What are main objects?

* policy (= loadable binary file with compiled set of rules)
* rule (= which domain can perform which operation on a context)
* context (filesystem label, socket etc.)
* domain (= domain is only switched on exec. When init execs apache, context is switched so that apache is "limited" by the policy)

---

## How is selinux implemented?

* in kernelspace, there is a layer between syscall interface and backend, e.g. we have a syscall read, which is backed by VFS (for filesystem FD)
* it is called linux security module (LSM)
* LSM is not only for selinux, AppArmor uses it as well
* there is loadable part called Security policy
* on boot, policy is loaded by dracut
* available policies are in packages selinux-policy-*

---

## Where are selinux (meta)data stored?

* in extended attributes
* in virtual filesystem selinuxfs mounted on /selinux

## and How to show those metadata?


    [jl@centosx /]$ getfattr -n security.selinux root home
    # file: root
    security.selinux="system_u:object_r:admin_home_t:s0"

    # file: home
    security.selinux="system_u:object_r:home_root_t:s0"

    [root@centosx ~]# ls -Zd /home
    drwxr-xr-x. root root system_u:object_r:home_root_t:s0 /home

---


## How to test what would be blocked by selinux?

    tail -f -n0 /var/log/messages /var/log/audit/audit.log
    setenforce 0 #(switch to permissive)
    ./perform_some_work.sh
    setenforce 1

## why we have switched to permissive here?

* logging in enforcing will stop after first denied
* normally, workload may need more than one permission

---

## What default policies are available from vendor?

They are in /etc/selinux/${POLICY}

* targeted (default)
* minimum
* mls

---

## Is is possible to fine tune available policies?

Policies often have tuneable booleans:

    getsebool -a | grep ftp
    setsebool -P ftp_home_dir on
    setsebool -P ftpd_use_passive_mode on

-P is important, as it means "Permanently", otherwise it is reinitialized on next boot.

---

## How to switch context of a file?

Until next relabel (for testing ...):

    chcon -rv --type httpd_sys_content_t /home/jl/html

Make it permanent:

    semanage fcontext -a -t httpd_sys_content_t "/html(/.*)?".

---

## How to list available contexts?

    chcon -Rv --type mock_var_shit_t /var/shit

it fails, because `mock_var_shit_t` is not known

    seinfo -t

lists available contexts.

---

## How to add custom context?

It can be added as a part of custom policy

    policy_module(mock, 1.0.0)

    type mock_var_shit_t;
    files_type(mock_var_shit_t)

We will compile and load this later.

---

## How to use audit2allow?

    # grep ftpd_t /var/log/audit/audit.log | \
      audit2allow -M ftpd_var_ftp
    # ls ftpd_var_ftp*
    ftpd_var_ftp.pp  ftpd_var_ftp.te
    # semodule -i ftpd_var_ftp.pp

as you see, audit2allow also compiles "te" file into a policy package (.pp)

--- 

## What if I want edit the policy before loading?

    #make a new folder, with only one *.te file
    mkdir pol; cd pol
    editor ftpd_var_ftp.te
    ln -s /usr/share/selinux/devel/Makefile
    make


## What if I want to remove previously loaded policy?

    semodule -r ftpd_var_ftp

* now I can install a new policy package if needed

---

## My policy

Allows upload of new files using webdav, but download and directory listing is not allowed.

    module httpd_var_shit 1.0;

    require {
        type httpd_t;
        type mock_var_shit_t;
        class file { rename write getattr create open unlink};
        class dir { write remove_name add_name};
    }

    #============= httpd_t ==============
    allow httpd_t mock_var_shit_t:dir { write add_name remove_name};
    allow httpd_t mock_var_shit_t:file { rename write getattr create open unlink};



---

* FIN
* THE END