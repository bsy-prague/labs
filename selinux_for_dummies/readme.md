# Selinux for dummies

Resources:
http://www.ibm.com/developerworks/library/l-selinux/
http://danwalsh.livejournal.com/24750.html

## What is SELinux?

* brought by NSA :-)
* provides access controll beyond traditional unix user/group/other permissions (often called DAC)
  http://en.wikipedia.org/wiki/Discretionary_access_control
* MAC (Mandatory access control)
* TE (Type enforcement)
* RBAC (Role based access control)
* MLS (Multilevel security)

## How is selinux implemented?

* in kernelspace, there is a layer between syscall interface and backend, e.g. we have a syscall read, which is backed by VFS (for filesystem FD)
* it is called linux security module (LSM)
* LSM is not only for selinux, AppArmor uses it as well
* there is loadable part called Security policy
* on boot, policy is loaded by dracut
* available policies are in packages selinux-policy-*

## How to tell if selinux is running?

getenforce

## How to switch off selinux completely?

setenforce 0 from command line
selinux=0 from kernel command linux


## Where are selinux (meta)data stored?

* in extended attributes
* in virtual filesystem selinuxfs mounted on /selinux

## How to show extended attributes?

[jl@centosx /]$ getfattr -n security.selinux root home
# file: root
security.selinux="system_u:object_r:admin_home_t:s0"

# file: home
security.selinux="system_u:object_r:home_root_t:s0"

* ls -Z 
* user:role:type

* ps axZ

## How to test what would be blocked by selinux?

* tail -f /var/log/messages
* setenforce 0 (switch to permissive)
* perform work
* setenforce 1

### why to switch to permissive? Messages are logged in enforcing as well?

* logging in eforcing will stop after first denied
* normally, workload may need more than one permission


## What are main objects?

* policy (= loadable binary file with compiled set of rules)
* rule (= which domain can perform which operation on a context)
* context (filesystem label, socket etc.)
* domain (= domain is only switched on exec. When init execs apache, context is switched so that apache is "limited" by the policy)

## What default policies are available from vendor?

They are in /etc/selinux/${POLICY}

* targeted (default)
* minimum
* mls

## Is is possible to fine tune available policies?

* policies often have tuneable booleans:
getsebool -a | grep ftp
setsebool -P ftp_home_dir on
setsebool -P ftpd_use_passive_mode on

-P is important, as it means "Permanently", otherwise it is reinitialized on next boot.

## How to switch context of a file?

* until next relabel (for testing ...)
chcon -rv --type httpd_sys_content_t /home/jl/html
* make it permanent
semanage fcontext -a -t httpd_sys_content_t "/html(/.*)?" 

## How to create own policy?

* start by downloading policy source:
git clone git://git.fedorahosted.org/selinux-policy.git
git checkout rawhide-base

* or user "Reference policy"
* https://github.com/TresysTechnology/refpolicy


Misc
* You can use the mount -o context= command to set a single context for an entire file system
* semanage login -l


Others
* apparmor (runs on LSM as well)
* trustedBSD


dracut FATAL: Initial SELinux policy load failed.



* How to allow ftp in /var/ftp?
* make a new type


## How to list available contexts?

## How to add custom context?


## How to list loaded policy modules?


file types
* .pp aka policy package -> binary policy
* .te aka type enforcement -> rules in text format

semodule -i mycertwatch2.pp


## How to use audit2allow?

* [root@centosx ~]# grep ftpd_t /var/log/audit/audit.log | audit2allow -M ftpd_var_ftp
* [root@centosx ~]# ls ftpd_var_ftp*
  ftpd_var_ftp.pp  ftpd_var_ftp.te
* [root@centosx ~]# semodule -i ftpd_var_ftp.pp

as you see, audit2allow also compiles "te" file into a policy package (.pp)

## What if I want edit the policy before loading?

* make a new folder, with only one *.te file
* editor ftpd_var_ftp.te
* ln -s /usr/share/selinux/devel/Makefile
* make

## What if I want to remove previously loaded policy?

* semodule -r ftpd_var_ftp
* now I can install a new policy package if needed

