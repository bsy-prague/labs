<link rel="stylesheet" href="github.css"></link>

# DOS

- Resource exhaustion
- Bandwidth
- Connections
- Algorithmic complexity (backtracking based regexes, DB heavy)

## TCP

- SYN flood (half-open connections)

### HTTP

- Faking Content-Length header
- Slowloris

#### Slowloris

- Slowly sending http POST
- Apache, max 150 processes
- Mitigation (limit per IP, switch to non-vulnerable HTTP server)

## UDP

- tiny packets (exhausting connections on target app)
- large packets (bandwidth, needs more bw than target)
- fake dns responses
  mz eth0 -A 83.167.232.167 -B 83.167.232.33 -t dns "q=google.com, a=1.2.3.4" -c 100
  (intentionaly broken example)

### Amplification

- More bandwidth is hard to get, distribute the attack
- Use amplification -> small request produces a larger response

## Defenses

- Stateful firewall (tracking connections, identify the attack properties, filter specific packets)
- Blackhole the source IP on upstream provider


1. Mousezahn
   - apt install mz
