Network data analysis

- netflow / sflow / ipfix
- packet captures (switch monitoring like RMON, local captures)
  WIRESHARK
- proxy logs

Our demos:
- tcpdump & tcptrace
  # dump data into files rotated each 15 minutes
  tcpdump -vv -n -i eth0 -f 'tcp port 80' -w "%F_%T.cap" -G 900

  pcapfile='2017-10-25_17:36:07.cap'
  # use tcptrace to extract information from trace
  tcptrace --csv -xHTTP -f'port=80' -lten ${pcapfile}

  #split cap file by tcp streams using tshark:
  for stream in `tshark -r ${pcapfile} -T fields -e tcp.stream | sort -n | uniq`;
  do
    tshark -r ${pcapfile} -w stream-$stream.cap -2 -R "tcp.stream==$stream"
  done

- simple analytics on netflow data

  router with netflow export (mikrotik in my case)
  nfacctd
  mysql

  select round(sum(bytes)/1024/1024) as c,ip_dst from d_20171103 group by ip_dst having c > 10 order by c
  select count(ip_dst) as c,ip_src from d_20171103 where inet_aton(ip_src) & inet_aton('255.255.0.0') = inet_aton('10.6.0.0') group by ip_src  having c > 10000 order by c;

- advanced analytics on netflow

  Apply for job at cisco :-)

- africa latency


Your tasks

- install wireshark
- find full tls handshake
- find session resumption handshake

- try at home:
https://jimshaver.net/2015/02/11/decrypting-tls-browser-traffic-with-wireshark-the-easy-way/
