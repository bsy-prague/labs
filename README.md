# BSY lab

[list of topics](./topics.txt)

## Topic assignments

### By date

| Date   | Lab | Name | Topic |
| -----  | --- | ---- | ----- |
| 12.10  | 1 | Hanč | T3    |
| 12.10  | 2 | Ščuková | T15 |
| 19.10. | 2 | Novák M. | T16 |
| 19.10. | 1 | Cicvárek | FDE |
| 12.10  | 1 | Pikous | T14 |
| 19.10. | 2 | Kratochvíl, Elena | T7 |
| 19.10. | 2 | Hogenauer | T9 |
| 26.10. | 1 | Mishchenko| OAuth |
| 26.10. | 1 | Knap | T8 |
| 26.10. | 2 | Samek | T8 |
| 26.10. | 2 | Musilová | T11 |
| 2.11. | 1 | Begera | T1 |
| 2.11. | 2 | Rolandová, Čuka | T18 |
| 2.11. | 2 | Dinara, Guzel, Hejná | T17 |
| 2.11. | 1 | - | - |
| 9.11. | 2 | Štercl | DNS/Firewall list |
| 9.11. | 1 | Tran | T7 |
| 9.11. | 1 | Kalodkin | Buffer overflow |
| 9.11. | 2 | Alisher | Homomorphic encryption |
| 16.11. | 2 | Novotný | T7 |
| 16.11. | 1 | Křečková | T1 |
| 16.11. | 1 | Šindelář | T5 |
| 16.11. | 2 | Klimeš, Štíbal | T14 |
| 23.11. | 1 | Ubr | T9 |
| 23.11. | 1 | Štec, Gabriel| T7 |
| 23.11. | 2 | Moravec, Kuchár | T16 |
| 23.11. | 2 | Petrovichev | T8 |
| 30.11. | 1 | Nikolai, Ildar | DES cracker |
| 30.11. | 2 | Bakeyev | JWT |
| 30.11. | 1 | Korekov | Passwords in DB history, bcrypt, pbkdf2 |
| 30.11. | 2 | Ryšavý | Context aware security |
| 7.12. | 1 | Figura, Kožár | (T14) |
| 7.12. | 1 | Lamper | T12 |
| 7.12. | 2 | Paulík | Copperhead OS |
| 7.12. | 2 | Slavíček | T17 |
| 14.12. | 1 | Passler | DNS Log malware detection |
| 14.12. | 2 | Jiran | Password managers |
| 14.12. | 2 | Matyáš | T19 |
| 21.12. | 2 | Marat | Diffe Hellman - MITM attack |
| 21.12. | 2 | Igor Kotov | IoT securiry |
| 21.12. | 2 | Perera | T23 |
| 21.12. | 2 | Dave | T22 |

### By topic

You can chose a topic that is already taken. If it is presented in the
same lab, you can either choose to cooperate, or compete.

* Cooperate: the talk/demo should be more in depth.
* Compete: your talk/demo will be rated against the other one.

You can change topics, you can come up w/ your own.

#### Own / custom

| Topic | Date | Lab | Name |
| ----- | ---- | --- | ---- |
| Buffer overflow | 9.11. | 2 | Kalodkin|
| Context aware security | 30.11. | 2 | Ryšavý |
| Copperhead OS | 7.12. | 2 | Paulík |
| DES cracker | 30.11. | 1 | Nikolai, Ildar |
| DNS Log malware detection | 14.12. | 1 | Passler |
| DNS/Firewall list | 9.11. | 1 | Štercl |
| FDE | 19.10. | 1 | Cicvárek |
| Homomorphic encryption | 9.11. | 2 | Alisher |
| JWT | 30.11. | 2 | Bakeyev|
| OAuth| 26.10. | 1 | Mishchenko |
| Password managers | 14.12. | 2 | Jiran |
| Passwords in DB history, bcrypt, pbkdf2 | 30.11. | 1 | Korekov |


| Topic | Date | Lab | Name |
| ----- | ---- | --- | ---- |
| T1 | 16.11. | 1 | Křečková |
| T1 | 2.11. | 1 | Begera |
| T3 | 12.10  | 1 | Hanč |
| T5 | 16.11. | 1 | Šindelář |
| T7 | 16.11. | 2 | Novotný |
| T7 | 19.10. | 2 | Kratochvíl, Elena |
| T7 | 23.11. | 1 | Štec, Gabriel|
| T7 | 9.11. | 1 | Tran |
| T8 | 23.11. | 2 | Petrovichev|
| T8 | 26.10. | 1 | Knap|
| T8 | 26.10. | 2 | Samek|
| T9 | 19.10. | 2 | Hogenauer |
| T9 | 23.11. | 1 | Ubr |
| T11 | 26.10. | 2 | Musilová |
| T12 | 7.12. | 1 | Lamper |
| T14 | 12.10  | 1 | Pikous |
| T14 | 14.12. | 1 | Stíbal |
| T14 | 7.12. | 1 | Klimeš |
| (T14) | 7.12. | 1 | Figura, Kožár |
| T15| 12.10  | 2 | Ščuková |
| T16 | 19.10. | 1 | Novák M |
| T16 | 23.11. | 2 | Moravec, Kuchár|
| T17 | 7.12. | 2 | Slavíček |
| T17| 2.11. | 2 | Dinara, Guzel, Hejná |
| T18| 2.11. | 2 | Rolandová, Čuka |
| T19 | 14.12. | 2 | Matyáš |
| T22 | 21.12. | 2 | Dave |
| T23 | 21.12. | 2 | Perera |
