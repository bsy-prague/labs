#!/usr/bin/env ruby

require 'sinatra'
require 'redis'
require 'securerandom'

# curl -D @filename http://localhost:4567/file

get '/' do
  redis = Redis.new
  redis.keys.join(', ')
end

post '/file' do
  unless params[:file] &&
         (tmpfile = params[:file][:tempfile]) &&
         (name = params[:file][:filename])
    STDERR.puts 'No file selected'
    return
  end
  STDERR.puts "Uploading file, original name #{name.inspect}"
  our_name = SecureRandom.hex
  File.open("/var/uploads/#{our_name}", 'w') do |f|
    while blk = tmpfile.read(65_536)
      f.puts blk
    end
  end
  redis = Redis.new
  redis.lpush 'orig_name', name
  redis.lpush 'our_names', our_name
  'Done.'
end
